Utils for ELEXIS Dictionary Viewer
=====================

This is a public repository contains utilities for registering, importing, parsing, indexing, rendering of dictionary data.

 Used by 
 
 * [elexis-dict-viewer-importer](https://bitbucket.org/tcdh/elexis-dict-viewer-importer)

 * [elexis-dict-viewer-management](https://bitbucket.org/tcdh/elexis-dict-viewer-management)
 
 * [tcdh-dict-api](https://bitbucket.org/tcdh/tcdh-dict-api)

For Production:

```
# every time changed tpyes or functions in dir src/, a new biuld is needed
npm run build

# not necessary, but recommended: change version number in package.json

```
