
export interface DictItem {
  id?: string; // hash of name
  name?: string;
  sigle?: string;
  gitURL?: string;
  branch?: string;
  entityFile?: string;
  dataFileExtension: string;
  dataDir?: string;
  reportTo?: string;
  owner?: string; // email of user
  createTime?: string;
  importedTime?: string;
  changeTime?: string;
  publicationTime?: string;
  published: boolean;
  importing: boolean;
  reindexing: boolean;
  description?: string;
  organisation?: string;
  category?:string;
  internetAdress?: string;
  locked?: boolean;
  failed?:boolean;
  teiRulesModified?: boolean;
}

export interface UserInfo {
  email: string; // used as id
  username?: string;
  password: string;
  newPassword?: string;
  newEmail?: string;
  firstName?: string;
  name?: string;
  organisation?: string;
  pendingCode?: string; // for validation
  createTime?: string; // for checking if pendingCode still there after some time
  pwResetCode?: string; // for reseting forgotten password.
  pwResetTime?: string; // resetCode is only available for a certain amount of time.
}

export interface Res {
  msg: string;
}

export interface AuthRes extends Res {
  token?: string;
}

export interface RichAuthRes extends AuthRes {
  firstName?: string;
  name?: string;
  organisation?: string;
  username?: string; 
}

export interface ResetRes extends AuthRes {
  email: string;
}

export interface SearchableField {
  fieldName: string;
  text: string;
  normtext: string;
}

export interface DictDialogData {
  dictItem: DictItem;
  trigger: 'create' | 'edit' | 'delete' | 'import' | 'published';
  isPublished?: boolean;
}

export interface ProfileDialogData {
  userInfo: UserInfo;
  trigger: 'changeMail' | 'changePW' | 'changeProfileImage' | 'changeMetadata';
}

export interface ImportDialogData {
  dictItem: DictItem;
  trigger: 'reimport';
}

export interface TagUniqItem {
  tag: string;
  attrList: TagUniqItemAttr[];
  config: TagUniqItemConfig;
}

export interface TagUniqItemAttr {
  name: string;
  valueList: TagUniqItemAttrValue[];
  config: TagUniqItemAttrConfig;
}

export interface TagUniqItemAttrValue {
  value: string;
}

export interface TagUniqItemConfig {
  useSubList: boolean;
}

export interface TagUniqItemAttrConfig extends TagUniqItemConfig {
  showAttrValue: boolean;
}

export interface Scripttype {
  normalScript: boolean;
  supScript: boolean;
  subScript: boolean;
}

export interface DictConfigItemStyle {
  font: string;
  italic: boolean;
  weight: string;
  fontSize: string;
  fontSizeUnit: string;
  fontColor: string;
  display: boolean;
  uppercase: boolean;
  lowercase: boolean;
  smallcaps: boolean;
  scripttype: Scripttype;
  linebreakafter: boolean;
  linebreakbefore: boolean;
  letterspacing: boolean;
  textindent: number;
  borderStyle: number;
}

export interface DictConfigItem {
  name: string;
  id: number;
  asSearchableField: boolean;
  asLemma: boolean;
  showText: boolean;
  locked?: boolean;
  tags: TagUniqItem[];
  style: DictConfigItemStyle;
}

/** ----------------------
 * for dict api:
 * -----------------------
 */
export interface DictApiLemmaItem {
  normtext: string;
  text: string;
  id: string;
}

export interface DictApiEntryItem {
  entry: string;
  id: string;
}

export interface SearchBlock {
  operator: 'and' | 'or' | 'not';
  definition: string;
  field: string;
}



/** ----------------------
 * for viewer config:
 * -----------------------
 */
// TODO: this need to be designed more and more specifically. Here is only an intuitive composition.
export interface ViewerConfig {
  view: any;
  modules: ViewerModuleConfig[];
} 
export interface ViewerModuleConfig {
  moduleName: string;
  config: any;
  component: any;
  data: any;
}
