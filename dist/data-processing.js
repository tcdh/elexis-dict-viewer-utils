"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.changeEntityToMappedChar = exports.normalizeString = exports.getTextInObj = exports.findTagElementOfLastLevelOfConfig = exports.checkObjByTagList = exports.checkTagWithAttr = void 0;
const he_1 = __importDefault(require("he"));
const charmap_1 = require("./charmap");
const cleanmap_1 = require("./cleanmap");
const cleanmapNoStar_1 = require("./cleanmapNoStar");
/**
 * check if given obj contains attrs and values as in the given attr list
 *
 * @param {TagUniqItemAttr[]} attrList
 * @param {any} obj
 * @returns {boolean}
 */
exports.checkTagWithAttr = (attrList, obj) => {
    let allAttrMatch = true;
    for (const configAttr of attrList) {
        if (!obj.attr[configAttr.name]) { // one of the config attr not in this tag
            allAttrMatch = false;
            break;
        }
        if (!configAttr.valueList.find(a => a.value === obj.attr[configAttr.name])) { // attr value in config not found in this tag
            allAttrMatch = false;
            break;
        }
    }
    return allAttrMatch;
};
/**
 * check if given obj contains tags specified as in the given tag list
 *
 * @param {TagUniqItem[]} tagUniqItemList config list of tags
 * @param {any} obj
 * @param {any} parentList
 * @returns {boolean}
 */
exports.checkObjByTagList = (tagUniqItemList, obj, parentList) => {
    let ret = true;
    if (tagUniqItemList.length > parentList.length) {
        return false;
    }
    for (let i = tagUniqItemList.length - 2, parentListI = parentList.length - 1; i >= 0; i--, parentListI--) {
        if (tagUniqItemList[i].tag !== parentList[parentListI].tag.replace(/__\d+$/, '')) {
            ret = false;
            break;
        }
        if (!exports.checkTagWithAttr(tagUniqItemList[i].attrList, parentList[parentListI].obj)) {
            ret = false;
            break;
        }
    }
    return ret;
};
/**
 * find tag elements specified as in given tag config list; and store them in param <resList>
 *
 * @param {TagUniqItem[]} tagUniqItemList config list of tags
 * @param {any} obj
 * @param {string} tag name of tag
 * @param {any} parentList
 * @param {any[]} resList list for storing found elements
 */
exports.findTagElementOfLastLevelOfConfig = (tagUniqItemList, obj, tag, parentList, resList) => {
    /*_________________________
    Eigendlich muss in obj das entry noch reingegeben werden an ders telle
    _________________________*/
    //console.log("tag: "); console.log(tag);
    //tagUniqItemList z.b.: 
    /*dictConfigItem.tags
    [ { tag: 'entry', config: { useSubList: true }, attrList: [] } ]
    */
    /*
    Beispiel zwei tagUniqItemList:
    [{"tag":"entry","attrList":
      [{"name":"type","valueList":
        [{"value":"sublemma"}],
      "config":{"showAttrValue":false,"useSubList":true}}],
    "config":{"useSubList":true}}]
  
    */
    console.log("anfangfindTag");
    console.log("obj");
    console.log(JSON.stringify(obj));
    console.log("parentList");
    console.log(JSON.stringify(parentList));
    console.log("tag: ");
    console.log(JSON.stringify(tag));
    console.log("tagUniqItemList");
    console.log(JSON.stringify(tagUniqItemList));
    console.log("resList");
    console.log(JSON.stringify(resList));
    const localParentList = [];
    for (const p of parentList) {
        localParentList.push(p);
    }
    localParentList.push({ obj, tag });
    console.log("localParentListNachAufbau");
    console.log(JSON.stringify(localParentList));
    const lastTagUniqItem = tagUniqItemList[tagUniqItemList.length - 1];
    console.log("lastTagUniqItem");
    console.log(JSON.stringify(lastTagUniqItem));
    for (const t in obj) {
        if (obj[t] && typeof obj[t] === 'object' && t !== 'attr') {
            //falls das obj existiert und es ein object ist und es kein attr ist. (ist attr das tiefste element?)
            let sameObjLikeInConfig = false;
            //folgende Zeile ergänzt, damit auch entry gestyled werden kann:
            if ((t.replace(/__\d+$/, '') === lastTagUniqItem.tag) || lastTagUniqItem.tag == "entry") {
                //falls das t aus obj das selbe als lastTagUniqItem.tag ist
                const allAttrMatch = exports.checkTagWithAttr(lastTagUniqItem.attrList, obj[t]);
                console.log("_______________________allAttrMatch");
                console.log("lastTagUniqItem.attrList");
                console.log(JSON.stringify(lastTagUniqItem.attrList));
                console.log("obj an stelle t");
                console.log(JSON.stringify(obj[t]));
                console.log("allAttrMatch");
                console.log(allAttrMatch);
                console.log("_______________________");
                /*Beispiel allAttrMatch:
        
                _______________________allAttrMatch
                lastTagUniqItem.attrList
                [{"name":"type","valueList":[{"value":"lemma"}],"config":{"showAttrValue":false,"useSubList":true}}]
                obj an stelle t
                {"attr":{"type":"headword"},"text__0":"\r\n\t\t\t\t\t","form__0":{"attr":{"type":"lemma"},"text__0":"âʒ-rëht"}}
                allAttrMatch
                false
                _______________________
        
                _______________________allAttrMatch
                lastTagUniqItem.attrList
                [{"name":"type","valueList":[{"value":"lemma"}],"config":{"showAttrValue":false,"useSubList":true}}]
                obj an stelle t
                {"attr":{"type":"lemma"},"text__0":"âʒ-rëht"}
                allAttrMatch
                true
                _______________________
        
                */
                if (allAttrMatch || lastTagUniqItem.tag == "entry") { //defakto, wenn es entry ist unterscheidet man nicht nach typen. (workaround)
                    //console.log("allAttrMatch"); console.log(allAttrMatch);
                    sameObjLikeInConfig = exports.checkObjByTagList(tagUniqItemList, obj[t], localParentList);
                    console.log("_______________________sameObjLikeInConfig");
                    console.log("tagUniqItemList");
                    console.log(JSON.stringify(tagUniqItemList));
                    console.log("obj an stelle t");
                    console.log(JSON.stringify(obj[t]));
                    console.log("localParentList");
                    console.log(JSON.stringify(localParentList));
                    console.log("_______________________");
                    if (sameObjLikeInConfig || lastTagUniqItem.tag == "entry") {
                        //console.log("sameObjLikeInConfig"); console.log(sameObjLikeInConfig);
                        resList.push(obj[t]);
                    }
                }
            }
            // disable this 'if' to find all children which also fit the defintion
            if (!sameObjLikeInConfig && !(lastTagUniqItem.tag == "entry")) {
                console.log("NOT sameObjLikeInConfig");
                exports.findTagElementOfLastLevelOfConfig(tagUniqItemList, obj[t], t, localParentList, resList);
            }
        }
    }
    /*Beispiel für Reslist:
  
    [{"attr":{"type":"headword"},"text__0":"\r\n\t\t\t\t\t","form__0":{"attr":{"type":"lemma"},"text__0":"Aʒʒabê"}},
      {"attr":{},"ref__0":{"attr":{"type":"BMZ","n":"1.74.b","target":"BA00844"},
      "text__0":"(I. 74","hi__0":{"attr":{"rend":"sup"},"text__0":"b"},"text__1":")"}},
      {"attr":{},"text__0":"\r\n\t\t\t\t\t\t","title__0":{"attr":{"n":"QB0035","type":"sigle"},
      "bibl__0":{"attr":{},"author__0":{"attr":{},"text__0":"Bit."}},"text__0":" ","ref__0":{"attr":{},"text__0":"1161"}},
      "text__1":" ","hi__0":{"attr":{"rend":"italic"},"text__0":"ist in"},"text__2":" Alzabe zu ",
      "hi__1":{"attr":{"rend":"italic"},"text__0":"ändern vgl."},"text__3":"\r\n\t\t\t\t\t\t",
      "title__1":{"attr":{"type":"sigle"},"bibl__0":{"attr":{},"author__0":{"attr":{},"text__0":"Jänickes"}},
      "text__0":" ","ref__0":{"attr":{},"hi__0":{"attr":{"rend":"italic"},"text__0":"anm. zu der stelle."}}},
      "text__4":"\r\n\t\t\t\t\t"}]
  
  */
    //console.log("resList"); console.log(JSON.stringify(resList)); //evtl muss man bei entry tagUniqItemList.tag mit tag vergleichen und dann das gesamte obj zurückgeben oder das verarbeitete???
    return resList;
};
/**
 * get text of given obj
 *
 * @param {any} obj
 * @param {string} resText
 * @param {string} normResText
 * @returns {string[]}
 */
exports.getTextInObj = (obj, resText, normResText) => {
    for (const t in obj) {
        if (obj[t] && typeof obj[t] === 'string' && t.replace(/__\d+$/, '') === 'text') {
            resText += he_1.default.decode(' ' + obj[t].trim());
            //normResText += ' ' + changeEntityToMappedChar(obj[t].trim());//<- alt
            //normalisierung temporär rausgenommen
            normResText += ' ' + exports.changeEntityToMappedChar(exports.normalizeString(obj[t].trim()));
        }
        else if (obj[t] && typeof obj[t] === 'object' && t !== 'attr') {
            const res = exports.getTextInObj(obj[t], resText, normResText);
            resText = res[0];
            normResText = res[1];
        }
    }
    //console.log(getTextInObj);
    //console.log("resText"); console.log(resText); console.log("normResText"); console.log(normResText);
    return [resText /*.trim()*/, normResText /*.trim()*/];
};
exports.normalizeString = (text, ignoreStar, notRemoveSpaces) => {
    //Defaults: dont ignore stars, remove Spaces
    //console.log("normalizeString text vorher"); console.log(text);
    let newtext = '';
    //console.log("normalizestring:"+text+"FERTIG");
    for (let i = 0; i < text.length; i++) {
        // character can be cleaned?
        if (ignoreStar == undefined || ignoreStar == false) {
            //console.log("ignorestar nicht da");
            if (cleanmap_1.cleanmap[text[i]] != undefined) {
                continue;
            }
        }
        if (ignoreStar) {
            //console.log("ignorestar aufgerufen, text an i");console.log(text[i]);
            if (cleanmapNoStar_1.cleanmapNoStar[text[i]] != undefined) {
                //console.log("wird geskipt"); console.log(text[i]);
                continue;
            }
        } /*else {
           if (cleanmap[text[i]] != undefined) {
              continue
           }
        }*/
        /*if(text =="Aʒʒabê") {
         let char1 = text.charCodeAt(i);
         console.log("charcodeatI"); console.log(char1);
         let hexcode41 = '0000'+char1.toString(16);
         console.log("hexcode4"); console.log(hexcode41);
         let hexcode1 = hexcode41.substr(hexcode41.length-4);
         console.log("hexcode"); console.log(hexcode1);
         console.log("charmap[hexcode]"); console.log(charmap[hexcode1]);
         console.log("text an i"); console.log(text[i]);
       }*/
        let char = text.charCodeAt(i);
        let hexcode4 = '0000' + char.toString(16);
        let hexcode = hexcode4.substr(hexcode4.length - 4);
        if (notRemoveSpaces == undefined || notRemoveSpaces == false) { //==removeSpaces
            //console.log("remove spaces");
            if (hexcode == '0020') { //=leerzeichen
                //console.log("leerzeichen gefunden");
                hexcode = '002E'; //=punkt
            }
        }
        if (text[i] == ' ') {
            //console.log("leerzeichen"); console.log(char); console.log("hexvon"); console.log(hexcode);
        }
        //if(text[i]=="*"){console.log("hexcode von stern");console.log(hexcode);console.log("charmap an hexcode");console.log(charmap[hexcode]);}
        if (charmap_1.charmap[hexcode] != undefined) {
            if (hexcode == '002E') { //punkt relation zu paar zeilen drüber dem if.
                newtext += (' ' + charmap_1.charmap[hexcode]);
            }
            else {
                newtext += charmap_1.charmap[hexcode];
            }
            //console.log("newtext += charmap[hexcode]:"+charmap[hexcode]+"FERTIG");
        }
        else {
            //console.log("newtext += text[i]:"+text[i]+"FERTIG");
            newtext += text[i];
        }
    }
    if (notRemoveSpaces == undefined || notRemoveSpaces == false) { //==removeSpaces
        //console.log("ich füg einen punkt hinzu");
        newtext = '.' + newtext;
    }
    //console.log("neuertext"); console.log(newtext);
    return newtext;
};
/**
 * change entities in given str to chars
 *
 * @param {string} str
 * @returns {string}
 */
exports.changeEntityToMappedChar = (str) => {
    //console.log("changeEntityToMappedChar vorher"); console.log(str);
    const findEntityReg = /&#x[a-fA-F0-9]{4,5};/g;
    let res = findEntityReg.exec(str);
    while (res) {
        let temp = res[0].replace('&#x', '').replace(';', '').toLowerCase();
        if (charmap_1.charmap[temp]) {
            //console.log("if fired");
            str = str.slice(0, res['index']) + charmap_1.charmap[temp]
                + str.slice(res['index'] + res[0].length);
            //console.log(str);
        }
        res = findEntityReg.exec(str);
    }
    //console.log("ergebnis"); console.log(str);
    return str;
};
