export interface DictItem {
    id?: string;
    name: string;
    gitURL: string;
    branch: string;
    entityFile?: string;
    dataFileExtension: string;
    dataDir: string;
    reportTo?: string;
    owner?: string;
    createTime?: string;
    published: boolean;
    importing: boolean;
    reindexing: boolean;
}
export interface UserInfo {
    email: string;
    password: string;
    name?: string;
    organisation?: string;
    pendingCode?: string;
    createTime?: string;
}
export interface Res {
    msg: string;
}
export interface AuthRes extends Res {
    token?: string;
}
export interface SearchableField {
    fieldName: string;
    text: string;
    normtext: string;
}
export interface DictDialogData {
    dictItem: DictItem;
    trigger: 'create' | 'edit' | 'delete' | 'import';
}
export interface TagUniqItem {
    tag: string;
    attrList: TagUniqItemAttr[];
    config: TagUniqItemConfig;
}
export interface TagUniqItemAttr {
    name: string;
    valueList: TagUniqItemAttrValue[];
    config: TagUniqItemAttrConfig;
}
export interface TagUniqItemAttrValue {
    value: string;
}
export interface TagUniqItemConfig {
    useSubList: boolean;
}
export interface TagUniqItemAttrConfig extends TagUniqItemConfig {
    showAttrValue: boolean;
}
export interface Scripttype {
    normalScript: boolean;
    supScript: boolean;
    subScript: boolean;
}
export interface DictConfigItemStyle {
    font: string;
    italic: boolean;
    weight: string;
    fontSize: string;
    fontSizeUnit: string;
    fontColor: string;
    display: boolean;
    uppercase: boolean;
    smallcaps: boolean;
    scripttype: Scripttype;
}
export interface DictConfigItem {
    name: string;
    asSearchableField: boolean;
    asLemma: boolean;
    showText: boolean;
    tags: TagUniqItem[];
    style: DictConfigItemStyle;
}
/** ----------------------
 * for dict api:
 * -----------------------
 */
export interface DictApiLemmaItem {
    normtext: string;
    text: string;
    id: string;
}
export interface DictApiEntryItem {
    entry: string;
    id: string;
}
export interface SearchBlock {
    operator: 'and' | 'or' | 'not';
    definition: string;
    field: string;
}
/** ----------------------
 * for viewer config:
 * -----------------------
 */
export interface ViewerConfig {
    view: any;
    modules: ViewerModuleConfig[];
}
export interface ViewerModuleConfig {
    moduleName: string;
    config: any;
    component: any;
    data: any;
}
