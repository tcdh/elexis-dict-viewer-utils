"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cleanmap = void 0;
exports.cleanmap = {
    //' ' : '',//punkt statt nix, damit ich später identifizieren kann wo leerzeichen waren. 
    ',': '',
    ':': '',
    ';': '',
    '(': '',
    ')': '',
    '{': '',
    '}': '',
    '\[': '',
    '\]': '',
    '?': '',
    '!': '',
    '"': '',
    '\'': '',
    '\\': '',
    '-': '',
    '+': '',
    '*': '',
    '=': '',
    '<': '',
    '>': '',
    '/': '',
    '&': '',
    '~': '',
    '|': '',
    '\#': '',
    '%': '',
    '.': '',
};
