"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.changeEntityToMappedChar = exports.getTextInObj = exports.findTagElementOfLastLevelOfConfig = exports.checkObjByTagList = exports.checkTagWithAttr = void 0;
const he_1 = __importDefault(require("he"));
const charmap_1 = require("./charmap");
/**
 * check if given obj contains attrs and values as in the given attr list
 *
 * @param {TagUniqItemAttr[]} attrList
 * @param {any} obj
 * @returns {boolean}
 */
exports.checkTagWithAttr = (attrList, obj) => {
    let allAttrMatch = true;
    for (const configAttr of attrList) {
        if (!obj.attr[configAttr.name]) { // one of the config attr not in this tag
            allAttrMatch = false;
            break;
        }
        if (!configAttr.valueList.find(a => a.value === obj.attr[configAttr.name])) { // attr value in config not found in this tag
            allAttrMatch = false;
            break;
        }
    }
    return allAttrMatch;
};
/**
 * check if given obj contains tags specified as in the given tag list
 *
 * @param {TagUniqItem[]} tagUniqItemList config list of tags
 * @param {any} obj
 * @param {any} parentList
 * @returns {boolean}
 */
exports.checkObjByTagList = (tagUniqItemList, obj, parentList) => {
    let ret = true;
    if (tagUniqItemList.length > parentList.length) {
        return false;
    }
    for (let i = tagUniqItemList.length - 2, parentListI = parentList.length - 1; i >= 0; i--, parentListI--) {
        if (tagUniqItemList[i].tag !== parentList[parentListI].tag.replace(/__\d+$/, '')) {
            ret = false;
            break;
        }
        if (!exports.checkTagWithAttr(tagUniqItemList[i].attrList, parentList[parentListI].obj)) {
            ret = false;
            break;
        }
    }
    return ret;
};
/**
 * find tag elements specified as in given tag config list; and store them in param <resList>
 *
 * @param {TagUniqItem[]} tagUniqItemList config list of tags
 * @param {any} obj
 * @param {string} tag name of tag
 * @param {any} parentList
 * @param {any[]} resList list for storing found elements
 */
exports.findTagElementOfLastLevelOfConfig = (tagUniqItemList, obj, tag, parentList, resList) => {
    const localParentList = [];
    for (const p of parentList) {
        localParentList.push(p);
    }
    localParentList.push({ obj, tag });
    const lastTagUniqItem = tagUniqItemList[tagUniqItemList.length - 1];
    for (const t in obj) {
        if (obj[t] && typeof obj[t] === 'object' && t !== 'attr') {
            let sameObjLikeInConfig = false;
            if (t.replace(/__\d+$/, '') === lastTagUniqItem.tag) {
                const allAttrMatch = exports.checkTagWithAttr(lastTagUniqItem.attrList, obj[t]);
                if (allAttrMatch) {
                    sameObjLikeInConfig = exports.checkObjByTagList(tagUniqItemList, obj[t], localParentList);
                    if (sameObjLikeInConfig) {
                        resList.push(obj[t]);
                    }
                }
            }
            // disable this 'if' to find all children which also fit the defintion
            if (!sameObjLikeInConfig) {
                exports.findTagElementOfLastLevelOfConfig(tagUniqItemList, obj[t], t, localParentList, resList);
            }
        }
    }
    return resList;
};
/**
 * get text of given obj
 *
 * @param {any} obj
 * @param {string} resText
 * @param {string} normResText
 * @returns {string[]}
 */
exports.getTextInObj = (obj, resText, normResText) => {
    for (const t in obj) {
        if (obj[t] && typeof obj[t] === 'string' && t.replace(/__\d+$/, '') === 'text') {
            resText += he_1.default.decode(' ' + obj[t].trim());
            normResText += ' ' + exports.changeEntityToMappedChar(obj[t].trim());
        }
        else if (obj[t] && typeof obj[t] === 'object' && t !== 'attr') {
            const res = exports.getTextInObj(obj[t], resText, normResText);
            resText = res[0];
            normResText = res[1];
        }
    }
    return [resText.trim(), normResText.trim()];
};
/**
 * change entities in given str to chars
 *
 * @param {string} str
 * @returns {string}
 */
exports.changeEntityToMappedChar = (str) => {
    const findEntityReg = /&#x[a-fA-F0-9]{4,5};/g;
    let res = findEntityReg.exec(str);
    while (res) {
        let temp = res[0].replace('&#x', '').replace(';', '').toLowerCase();
        if (charmap_1.charmap[temp]) {
            str = str.slice(0, res['index']) + charmap_1.charmap[temp]
                + str.slice(res['index'] + res[0].length);
        }
        res = findEntityReg.exec(str);
    }
    return str;
};
