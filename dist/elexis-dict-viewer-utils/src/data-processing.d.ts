import { TagUniqItemAttr, TagUniqItem } from './types';
/**
 * check if given obj contains attrs and values as in the given attr list
 *
 * @param {TagUniqItemAttr[]} attrList
 * @param {any} obj
 * @returns {boolean}
 */
export declare const checkTagWithAttr: (attrList: TagUniqItemAttr[], obj: any) => boolean;
/**
 * check if given obj contains tags specified as in the given tag list
 *
 * @param {TagUniqItem[]} tagUniqItemList config list of tags
 * @param {any} obj
 * @param {any} parentList
 * @returns {boolean}
 */
export declare const checkObjByTagList: (tagUniqItemList: TagUniqItem[], obj: any, parentList: {
    obj: any;
    tag: string;
}[]) => boolean;
/**
 * find tag elements specified as in given tag config list; and store them in param <resList>
 *
 * @param {TagUniqItem[]} tagUniqItemList config list of tags
 * @param {any} obj
 * @param {string} tag name of tag
 * @param {any} parentList
 * @param {any[]} resList list for storing found elements
 */
export declare const findTagElementOfLastLevelOfConfig: (tagUniqItemList: TagUniqItem[], obj: any, tag: string, parentList: any[], resList: any[]) => any[];
/**
 * get text of given obj
 *
 * @param {any} obj
 * @param {string} resText
 * @param {string} normResText
 * @returns {string[]}
 */
export declare const getTextInObj: (obj: any, resText: string, normResText: string) => string[];
/**
 * change entities in given str to chars
 *
 * @param {string} str
 * @returns {string}
 */
export declare const changeEntityToMappedChar: (str: string) => string;
