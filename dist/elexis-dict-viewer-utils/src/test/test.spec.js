"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("jasmine");
const data_processing_1 = require("../data-processing");
describe('Server behaviours:', () => {
    let originalTimeoutInterval = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    const tagUniqItemList = [
        {
            tag: 'a',
            attrList: [
                {
                    name: 'attr1',
                    valueList: [
                        {
                            value: 'attr1-value',
                        },
                    ],
                    config: {
                        showAttrValue: false,
                        useSubList: false,
                    }
                },
                {
                    name: 'attr2',
                    valueList: [
                        {
                            value: 'attr2-value',
                        },
                    ],
                    config: {
                        showAttrValue: false,
                        useSubList: false,
                    }
                },
            ],
            config: {
                useSubList: false,
            }
        },
        {
            tag: 'b',
            attrList: [
                {
                    name: 'attr1',
                    valueList: [
                        {
                            value: 'attr1-value',
                        },
                    ],
                    config: {
                        showAttrValue: false,
                        useSubList: false,
                    }
                },
                {
                    name: 'attr2',
                    valueList: [
                        {
                            value: 'attr2-value',
                        },
                    ],
                    config: {
                        showAttrValue: false,
                        useSubList: false,
                    }
                },
            ],
            config: {
                useSubList: false,
            }
        },
    ];
    beforeAll((done) => {
        originalTimeoutInterval = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;
        done();
    });
    afterAll((done) => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeoutInterval;
        done();
    });
    it('test checking obj with attr and attr values', (done) => {
        let obj = {
            a: {
                attr: {
                    attr1: 'attr1-value',
                    attr2: 'attr2-value',
                }
            }
        };
        expect(data_processing_1.checkTagWithAttr(tagUniqItemList[0].attrList, obj.a)).toBeTruthy();
        obj = {
            a: {
                attr: {
                    attr11: 'attr1-value',
                }
            }
        };
        expect(data_processing_1.checkTagWithAttr(tagUniqItemList[0].attrList, obj.a)).toBeFalsy();
        obj = {
            a: {
                attr: {
                    attr1: 'attr1-value',
                    attr2: 'attr2222-value',
                }
            }
        };
        expect(data_processing_1.checkTagWithAttr(tagUniqItemList[0].attrList, obj.a)).toBeFalsy();
        done();
    });
    it('test checking obj by reindexing config item', (done) => {
        let obj = {
            t: {
                a: {
                    attr: {
                        attr1: 'attr1-value',
                        attr2: 'attr2-value',
                    },
                    b: {
                        attr: {
                            attr1: 'attr1-value',
                            attr2: 'attr2-value',
                        }
                    }
                },
                a__0: {
                    attr: {
                        attr1: 'attr1-value',
                        attr2: 'attr2-value',
                    },
                    b: {
                        attr: {
                            attr1: 'attr1-value',
                            attr2: 'attr2-value',
                        }
                    }
                },
            }
        };
        expect(data_processing_1.checkObjByTagList(tagUniqItemList, obj.t.a.b, [{ obj: obj, tag: 'obj' }, { obj: obj.t, tag: 't' }, { obj: obj.t.a, tag: 'a' }])).toBeTruthy();
        expect(data_processing_1.checkObjByTagList(tagUniqItemList, obj.t.a.b, [{ obj: obj.t, tag: 't' }, { obj: obj.t.a, tag: 'a' }])).toBeTruthy();
        expect(data_processing_1.checkObjByTagList(tagUniqItemList, obj.t.a, [{ obj: obj.t, tag: 't' }])).toBeFalsy();
        expect(data_processing_1.checkObjByTagList(tagUniqItemList, obj.t.a__0.b, [{ obj: obj.t, tag: 't' }, { obj: obj.t.a__0, tag: 'a__0' }])).toBeTruthy();
        obj = {
            t: {
                a: {
                    attr: {
                        attr1: 'attr1-value',
                        attr22: 'attr2-value',
                    },
                    b: {
                        attr: {
                            attr1: 'attr1-value',
                            attr2: 'attr2-value',
                        }
                    }
                }
            }
        };
        expect(data_processing_1.checkObjByTagList(tagUniqItemList, obj.t.a.b, [{ obj: obj.t, tag: 't' }, { obj: obj.t.a, tag: 'a' }])).toBeFalsy();
        done();
    });
    it('test finding obj by config item', (done) => {
        let obj = {
            t: {
                a: {
                    attr: {
                        attr1: 'attr1-value',
                        attr2: 'attr2-value',
                    },
                    b: {
                        attr: {
                            attr1: 'attr1-value',
                            attr2: 'attr2-value',
                        },
                        text: 's',
                    },
                    text: 'aaa',
                },
                a__0: {
                    attr: {
                        attr1: 'attr1-value',
                        attr2: 'attr2-value',
                    },
                    b: {
                        attr: {
                            attr1: 'attr1-value',
                            attr2: 'attr2-value',
                        },
                        text: 'g',
                    },
                    text: 'a__0-aaa',
                },
            }
        };
        let resList = [];
        resList = data_processing_1.findTagElementOfLastLevelOfConfig(tagUniqItemList, obj, 'obj', [], resList);
        expect(resList.length).toBe(2);
        expect(resList[0].text).toBe('s');
        expect(resList[1].text).toBe('g');
        resList = [];
        resList = data_processing_1.findTagElementOfLastLevelOfConfig(tagUniqItemList, obj.t, 't', [], resList);
        expect(resList.length).toBe(2);
        expect(resList[0].text).toBe('s');
        expect(resList[1].text).toBe('g');
        obj = {
            t: {
                a: {
                    attr: {
                        attr1: 'attr1-value',
                        attr22: 'attr2-value',
                    },
                    b: {
                        attr: {
                            attr1: 'attr1-value',
                            attr2: 'attr2-value',
                        }
                    }
                }
            }
        };
        resList = [];
        resList = data_processing_1.findTagElementOfLastLevelOfConfig(tagUniqItemList, obj.t, 't', [], resList);
        expect(resList.length).toBe(0);
        done();
    });
    it('test getting text in obj', (done) => {
        let obj = {
            t: {
                text: 't1',
                a: {
                    attr: { text: 'ddddd' },
                    b: {
                        attr: {},
                        text: 's',
                    },
                    text: 'aaa',
                },
                text__0: 't2',
                a__0: {
                    b: {
                        text: 'g',
                        text__0: 'g__0',
                    },
                    text: 'a__0-aaa&#x002b;',
                },
            }
        };
        const res = data_processing_1.getTextInObj(obj, '', '');
        const text = res[0].trim();
        const normText = res[1].trim();
        expect(text.indexOf('t1')).toBe(0);
        expect(text.indexOf('t1 s aaa')).toBe(0);
        expect(text.indexOf('t1 s aaa t2 g g__0 a__0-aaa')).toBe(0);
        expect(normText.indexOf('t1 s aaa t2 g g__0 a__0-aaa+')).toBe(0);
        done();
    });
});
