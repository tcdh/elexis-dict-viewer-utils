
const gulp = require('gulp');
const { execSync } = require('child_process');

const typescripts = (done) => {
  execSync('node_modules/.bin/tsc');
  done();
};

const watch = () => {
  watchTs();
};

const watchTs = () => {
  return gulp.watch('src/**/*.ts', typescripts);
};

exports.watchTs = watchTs;
exports.typescripts = typescripts;

exports.watch = watch;
exports.build = gulp.series(typescripts);